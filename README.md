# Gerador de Certificados #

Customize o certificado seguindo o template, selecione a lista de email .csv e em seguida gere os PDFs em lote.

Scaffolded yo gulp-angular

### Instalação ###

* npm install
* bower install

### Comandos do gulp ###

* gulp serve
* gulp build

### Aceitamos contribuições para melhoria do código, segue lista do que desejo fazer ###

* Escrever testes automatizados com Jasmine
* Atribuir funcionalidades da geração de PDF ao backend, sugestão PHANTOMJS
* Realizar envio de email automáticamente
* Criar novos templates