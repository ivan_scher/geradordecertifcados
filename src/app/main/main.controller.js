(function() {
  'use strict';

  angular
  .module('geradordecertifcados')
  .controller('MainController', MainController);

  /** @ngInject */
  function MainController($http, $log, $timeout) {
    var vm = this;

    vm.responsable = "Ivan Scher";
    vm.role = "GDG Community Organizer";
    vm.texto = "participou do evento Google IO Extended Ribeirão Preto, dia inteiro de palestras, realizado na Faculdade Regis dia 18 de Maio de 2016 com a carga horária de 8 horas. ";
    vm.align = "center";
    vm.alignText ="left-align";
    vm.participante ="Fulano de Tal";


    vm.readURL = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          var myEl = angular.element('#previewHolder' );
          myEl.attr('src',e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    angular.element('#filePhoto').change(function(e) {
      vm.readURL(e.target);
    });

    //reade csv
    vm.start = function(){
      var Url   = "assets/lista.csv";
      $http.get(Url).then(function(response){
        var allTextLines = response.data.split(/\r\n|\n/);
        var headers = allTextLines[0].split(',');
        var lines = [];

        var teste = function(count){
          var data = allTextLines[count].split(',');
          if (data.length == headers.length) {
            var tarr = [];
            for ( var j = 0; j < headers.length; j++) {
              tarr.push(data[j]);
            }
            lines.push(tarr);
            var pdfname = tarr[1]+'.pdf';
            vm.participante = tarr[0];
            $log.info(tarr[0] + ' / ' + pdfname);
            vm.generatePDF( "#preview",pdfname);


            $timeout(function(){
              count++;
              teste(count);
            }, 2000);

          }
        }
        teste(0);

      });
    }


    //generate pdf
    vm.generatePDF = function(divID, PDFName){ 
      $log.info('generatePDF ' + PDFName)
      
      html2canvas(angular.element(divID), {
        removeContainer:true,
        onrendered: function (canvas) {   
          var data = canvas.toDataURL("image/png");                   
          var docDefinition = {
            pageSize: 'A4',
                    // by default we use portrait, you can change it to landscape if you wish
                    pageOrientation: 'landscape',
                    pageMargins: [0,0,0,0],
                    content: [{
                      image: data                        
                    }]
                  };
                  pdfMake.createPdf(docDefinition).download(PDFName);
                }
              });
      
    }
  }
})();
